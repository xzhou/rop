#include <stdio.h>
#include <string.h>

char* not_used = "/bin/sh";

//http://codearcana.com/posts/2013/05/28/introduction-to-return-oriented-programming-rop.html
//gcc simple_rop.c -o unsafe.a.out -fno-stack-protector

void not_called() {
  printf("enjoy your shell!");
  system("/bin/bash");
}

void vulnerable_function(char* string){
  char buffer[100];
  strcpy(buffer, string);

}

int main(int argc, char** argv){
  vulnerable_function(argv[1]);
  return 0;
}
